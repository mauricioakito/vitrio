import { Fragment } from 'react';
import Navbar from './components/layout/Navbar'
import Home from './components/pages/Home'
import Newsletter from './components/layout/Newsletter'
import Footer from './components/layout/Footer'

import './styles/App.scss';

const App = () => {

  return (
    <Fragment>
      <Navbar/>
      <Home />
      <Newsletter />
      <Footer />
    </Fragment>
  );
};

export default App;
