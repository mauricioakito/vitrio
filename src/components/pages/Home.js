import React from 'react';
import Cards from '../cards/Cards';

const Home = () => {
  return (
    <div className='container py-60'>
      <Cards />
    </div>
  );
};

export default Home;
