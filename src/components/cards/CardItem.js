import React from 'react';
import Button from '../button/Button';
import NumberFormat from 'react-number-format';

const CardItem = ({ data }) => {
  const images = data.items[0].images;
  const price = data.items[0].sellers[0].commertialOffer.Installments;

  return (
    <div className='card-item text-center'>
      <img
        src={images[0].imageUrl}
        alt={data.productName}
        className='card-image'
      />

      <h3 className='card-itemname'>{data.productName}</h3>

      {data.sobconsulta[0] === 'sim' ? (
        <Button className='card-button2' text='Consulte' />
      ) : (
        <>
          <NumberFormat
            className='card-itemprice'
            value={price[9].Value * price[9].NumberOfInstallments}
            displayType={'text'}
            thousandSeparator={'.'}
            decimalSeparator={','}
            prefix={'R$ '}
            suffix={',00'}
          />
          <NumberFormat
            className='card-itemcondition'
            value={price[9].Value}
            displayType={'text'}
            thousandSeparator={'.'}
            decimalSeparator={','}
            prefix={'10x de R$ '}
            suffix={',00 sem juros'}
          />
          <Button text='Comprar' />
        </>
      )}
    </div>
  );
};

export default CardItem;
