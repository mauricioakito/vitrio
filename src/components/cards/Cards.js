import catalogo from '../../data/catalogo.json';
import CardItem from './CardItem';

const Cards = () => (
  <div className='card-container'>
    {catalogo.map((item, key) => (
      <CardItem data={item} key={key} />
    ))}
  </div>
);

export default Cards;
