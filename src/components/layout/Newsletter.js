import React from 'react';
import { useForm } from 'react-hook-form';

const Newsletter = () => {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const onSubmit = (data) => console.log(data);

  console.log(watch('example'));

  return (
    <section className='newsletter'>
      <div className='container text-center'>
        <h2 className='newsletter-title'>Assine nossa newsletter</h2>
        <h3 className='newsletter-subtitle'>
          Fique por dentro das nossas novidades e receba 10% de desconto na
          primeira compra
        </h3>
        <h4 className='newsletter-rule'>
          * válido somente para joias e não acumulativo com outras promoções
        </h4>

        <form className='newsletter-form' onSubmit={handleSubmit(onSubmit)}>
          <input
            className='newsletter-input'
            placeholder='Seu nome'
            {...register('nameRequired', { required: true })}
          />

          <input
            type='email'
            className='newsletter-input'
            placeholder='Seu email'
            {...register('emailRequired', { required: true })}
          />

          {errors.nameRequired && (
            <span className='newsletter-nameinput'>
              Esse campo é necessário
            </span>
          )}
          {errors.emailRequired && (
            <span className='newsletter-emailinput'>
              Esse campo é necessário
            </span>
          )}

          <input className='newsletter-button' type='submit' />
        </form>
      </div>
    </section>
  );
};

export default Newsletter;
