import Logo from '../../images/logo.png';

const Navbar = () => {
  return (
    <div className='navbar'>
      <div className='container text-center'>
        <a href='http://localhost:3000' className='navbar-logolink'>
          <img className='navbar-logoimg' src={Logo} alt='Logo' />
        </a>
      </div>
    </div>
  );
};

export default Navbar;
