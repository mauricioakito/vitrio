import React from 'react';

const Button = ({ text }) => (
  <a href='http://localhost:3000' className='card-itemlink card-hide'>
    {text}
  </a>
);

export default Button;
