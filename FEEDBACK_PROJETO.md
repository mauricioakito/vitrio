# Feedback do projeto

## O que fiz

- Busquei executar todas as tasks seguindo ao máximo as instruçoes.

## O que não consegui fazer

- Não implementei tipagem no projeto por conta de ainda não conhecer Typescript.

## Considerações finais sobre o projeto

Eu senti que foi tranquilo, é algo que eu possuo familiaridade. Eu só não entendi a acerca do consumo dos dados do endpoint,
se era para consumir da url ou do arquivo anexado ao projeto. Eu acabei por consumir localmente.
